package com.solutions.myo.carbuddy.interfaces;

import com.solutions.myo.carbuddy.R;

/**
 * Created by Jacek on 2017-03-08.
 */

public interface ILocateModes {

    interface ILocateModeId{
        int LOCATE_WALLET = 101;
        int LOCATE_CAR = 102;
    }

    interface ILocateModeTitle{
        int LOCATE_WALLET = R.string.locate_mode_title_wallet;
        int LOCATE_CAR = R.string.locate_mode_title_car;
    }

    interface ILocateModeDescription{
        int LOCATE_WALLET = R.string.locate_mode_description_wallet;
        int LOCATE_CAR = R.string.locate_mode_description_car;
    }

}
