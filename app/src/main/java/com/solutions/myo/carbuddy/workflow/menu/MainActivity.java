package com.solutions.myo.carbuddy.workflow.menu;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.solutions.myo.carbuddy.R;
import com.solutions.myo.carbuddy.databinding.ActivityMainBinding;
import com.solutions.myo.carbuddy.interfaces.ILocateModes;
import com.solutions.myo.carbuddy.utils.PermissionUtils;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, View.OnClickListener, GoogleMap.OnMapClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private ActivityMainBinding binding;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    private GoogleMap mMap;

    private boolean mShowPermissionDeniedDialog = false;

    //TODO move to MarkerHelper
    private int MARKER_MODE;

    //TODO move to MarkerHelper
    private HashMap markerMap = new HashMap();

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    private Circle mCarCircle;

    private int ACCEPTABLE_DISTANCE_FOR_WALLET_FROM_CAR = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initViews();
        initMap();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initViews() {
        Toolbar toolbar = binding.appBarMain.toolbar;
        setSupportActionBar(toolbar);


        DrawerLayout drawer = binding.drawerLayout;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navView = binding.navView;
        navView.setNavigationItemSelectedListener(this);

        binding.appBarMain.fabMonitor.setOnClickListener(this);
        binding.appBarMain.locateModeLayoutSwitchContainer.btnLocateDone.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Unregister for location callbacks:
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Log.d(TAG, "onNavigationItemSelected");
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_locate_wallet) {
            if(mGoogleApiClient.isConnected()){
                mGoogleApiClient.disconnect();
            }
            setupLocateMode(ILocateModes.ILocateModeId.LOCATE_WALLET);
        } else if (id == R.id.nav_locate_car) {
            if(mGoogleApiClient.isConnected()){
                mGoogleApiClient.disconnect();
            }
            setupLocateMode(ILocateModes.ILocateModeId.LOCATE_CAR);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupLocateMode(int mode) {
        if (checkReady()) {
            binding.appBarMain.locateModeLayoutSwitchContainer.locateModeSwitchContainer.setVisibility(View.VISIBLE);
            mMap.setOnMapClickListener(this);
            //TODO save MARKER_MODE in MarkerHelper class
            MARKER_MODE = mode;
            setHitText(mode);
        }
    }

    private void disableLocateMode(int mode) {
        if (checkReady()) {
            binding.appBarMain.locateModeLayoutSwitchContainer.locateModeSwitchContainer.setVisibility(View.GONE);
            mMap.setOnMapClickListener(null);
        }
    }


    private void setHitText(int mode) {
        switch (mode) {
            case ILocateModes.ILocateModeId.LOCATE_WALLET:
                binding.appBarMain.locateModeLayoutSwitchContainer.tvLocateMode.setText(getString(R.string.tap_locate_wallet));
                break;
            case ILocateModes.ILocateModeId.LOCATE_CAR:
                binding.appBarMain.locateModeLayoutSwitchContainer.tvLocateMode.setText(getString(R.string.tap_locate_car));
                break;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        Toast.makeText(this, "buildGoogleApiClient", Toast.LENGTH_SHORT).show();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        updateMyLocation();
        buildGoogleApiClient();


//        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_retro));

    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.d(TAG, "onMapClick");
        checkIfMarkerExist(MARKER_MODE, latLng);
    }

    private void checkIfMarkerExist(int marker_mode, LatLng latLng) {
        Marker marker = (Marker) markerMap.get(marker_mode);
        if (marker != null) {
            updateMarker(marker, marker_mode, latLng);
        } else {
            createMarker(marker_mode, latLng);
        }

    }

    private void createMarker(int mode, LatLng latLng) {
        if (checkReady()) {
            switch (mode) {
                case ILocateModes.ILocateModeId.LOCATE_WALLET:
                    MarkerOptions optionsWallet = new MarkerOptions();
                    optionsWallet.position(latLng);
                    optionsWallet.title(getString(ILocateModes.ILocateModeTitle.LOCATE_WALLET));
                    optionsWallet.snippet(getString(ILocateModes.ILocateModeDescription.LOCATE_WALLET));
                    optionsWallet.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_account_balance_wallet_black_18dp));
                    Marker walletMarker = mMap.addMarker(optionsWallet);

                    updateHashMapWithMarker(ILocateModes.ILocateModeId.LOCATE_WALLET, walletMarker);

                    break;
                case ILocateModes.ILocateModeId.LOCATE_CAR:

                    MarkerOptions optionsCar = new MarkerOptions();
                    optionsCar.position(latLng);
                    optionsCar.title(getString(ILocateModes.ILocateModeTitle.LOCATE_CAR));
                    optionsCar.snippet(getString(ILocateModes.ILocateModeDescription.LOCATE_CAR));
                    optionsCar.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_directions_car_black_18dp));
                    Marker carMarker = mMap.addMarker(optionsCar);

                    updateHashMapWithMarker(ILocateModes.ILocateModeId.LOCATE_CAR, carMarker);
                    break;
            }
        }
    }

    private void updateHashMapWithMarker(int key, Marker newMarker) {
        markerMap.put(key, newMarker);
    }


    private void updateMarker(Marker marker, int mode, LatLng latLng) {
        marker.remove();
        markerMap.remove(mode);

        createMarker(mode, latLng);
    }

    private void updateMyLocation() {
        if (!checkReady()) {
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, false);
        }
    }

    private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, R.string.map_is_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, results,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
        } else {
            mShowPermissionDeniedDialog = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mShowPermissionDeniedDialog) {
            PermissionUtils.PermissionDeniedDialog
                    .newInstance(false).show(getSupportFragmentManager(), "dialog");
            mShowPermissionDeniedDialog = false;
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_locate_done:
                disableLocateMode(MARKER_MODE);
                break;

            case R.id.fab_monitor:

                Marker carMarker = (Marker) markerMap.get(ILocateModes.ILocateModeId.LOCATE_CAR);
                Marker walletMarker = (Marker) markerMap.get(ILocateModes.ILocateModeId.LOCATE_WALLET);

                if (carMarker != null && walletMarker != null) {
                    if(mCarCircle==null){
                        addGeoFenceForCar(carMarker);
                    }else{
                        removeCurrentGeoFenceForCar();
                        addGeoFenceForCar(carMarker);
                    }

                    mGoogleApiClient.connect();
                }

                break;
        }
    }

    private void removeCurrentGeoFenceForCar() {
        mCarCircle.remove();
    }

    private void addGeoFenceForCar(Marker carMarker) {
        LatLng carLatLng = carMarker.getPosition();

        CircleOptions circleOptions = new CircleOptions()
                .center(carLatLng)
                .radius(ACCEPTABLE_DISTANCE_FOR_WALLET_FROM_CAR)
                .fillColor(0x40ff0000)
                .strokeColor(Color.TRANSPARENT)
                .strokeWidth(2);

// Get back the mutable Circle
        mCarCircle = mMap.addCircle(circleOptions);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this,"onConnectionSuspended",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this,"onConnectionFailed",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        float[] currentLocationDistance = new float[2];

        Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                mCarCircle.getCenter().latitude, mCarCircle.getCenter().longitude, currentLocationDistance);

        if( currentLocationDistance[0] <= (mCarCircle.getRadius())  ){
            Toast.makeText(getBaseContext(), "You are by your car!", Toast.LENGTH_LONG).show();
            checkIfWalletIsInCar();

        } else {
//            Toast.makeText(getBaseContext(), "You are away from car", Toast.LENGTH_LONG).show();
        }

    }

    private void checkIfWalletIsInCar(){
        Marker carMarker = (Marker) markerMap.get(ILocateModes.ILocateModeId.LOCATE_CAR);
        Marker walletMarker = (Marker) markerMap.get(ILocateModes.ILocateModeId.LOCATE_WALLET);

        float[] carToWalletDistance = new float[2];

        Location.distanceBetween(carMarker.getPosition().latitude, carMarker.getPosition().longitude,
                walletMarker.getPosition().latitude, walletMarker.getPosition().longitude, carToWalletDistance);

        if( carToWalletDistance[0] > (ACCEPTABLE_DISTANCE_FOR_WALLET_FROM_CAR)  ){
            Toast.makeText(getBaseContext(), "You forgot about your wallet", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getBaseContext(), "You have your wallet, have a nice journey!", Toast.LENGTH_LONG).show();
        }

    }
}
